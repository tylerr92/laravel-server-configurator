#!/usr/bin/env bash

# Setup colors
success='\033[0;321m' # Green Color
warn='\033[1;33m'     # Yellow Color
error='\033[0;31m'    # Red Color
normal='\033[0m'      # No Color

# Intro
echo "Designed and Maintained by:"
echo "  _         _                 ___ ____  "
echo " | |_ _   _| | ___ _ __ _ __ / _ \___ \ "
echo " | __| | | | |/ _ \ '__| '__| (_) |__) |"
echo " | |_| |_| | |  __/ |  | |   \__, / __/ "
echo "  \__|\__, |_|\___|_|  |_|     /_/_____|"
echo "      |___/                             "

# TODO: Setup gitlab link and paypal donation link
echo "Find it on gitlat at: https://gitlab.com/tylerr92/laravel-server-configurator"
echo "Like the project? Support us by contributing!"
echo "Or make a donation to the support of this using paypal:"
# Todo: Add support for argument to override OS restriction
# Todo: Add support for Deb, Apt, and Pacman systems (Debian, Ubuntu, and Arch)
echo "Please note:"
echo "Currently only CentOS 7 is supported"

# Determine what OS and Version are running:
if [ -f /etc/os-release ]; then
  # freedesktop.org and systemd
  . /etc/os-release
  OS=$NAME
  VER=$VERSION_ID
elif type lsb_release >/dev/null 2>&1; then
  # linuxbase.org
  OS=$(lsb_release -si)
  VER=$(lsb_release -sr)
elif [ -f /etc/lsb-release ]; then
  # For some versions of Debian/Ubuntu without lsb_release command
  . /etc/lsb-release
  OS=$DISTRIB_ID
  VER=$DISTRIB_RELEASE
elif [ -f /etc/debian_version ]; then
  # Older Debian/Ubuntu/etc.
  OS=Debian
  VER=$(cat /etc/debian_version)
elif [ -f /etc/SuSe-release ]; then
  # Older SuSE/etc.
  ...
elif [ -f /etc/errorhat-release ]; then
  # Older Red Hat, CentOS, etc.
  ...
else
  # Fall back to uname, e.g. "Linux <version>", also works for BSD, etc.
  OS=$(uname -s)
  VER=$(uname -r)
fi

if [ "$OS" == 'CentOS Linux' ] && [ "$VER" == 7 ]; then
  printf "Operating System Is Supported %-20s [${success}OK"${normal}]
  echo ""
else
  printf "Operating System Is NOT Supported %-17s [${error}FAIL"${normal}]
  echo ""
  exit 1
fi

# Check if running as root or sudo
if [ "$EUID" -ne 0 ]
  then echo "Due to the nature of the configurations needed, this application must be run as root. Please switch to root or use sudo before running this script again."
  exit 1
fi

#Enable SELinux?
while true; do
  read -p "Do you want to use SELinux?" yn
  case $yn in
  [Yy]*)
    selinux=yes
    echo "SELinux will be configured"
    break
    ;;
  [Nn]*)
    selinux=no
    echo "SELinux will be disabled"
    break
    ;;
  *) echo "Please answer yes or no." ;;
  esac
done

#Enable Firewall?
while true; do
  read -p "Do you want to use the built in Firewall?" yn
  case $yn in
  [Yy]*)
    firewall=yes
    echo "The builtin firewall will be configured"
    break
    ;;
  [Nn]*)
    firewall=no
    echo "The builtin firewall will be disabled"
    break
    ;;
  *) echo "Please answer yes or no." ;;
  esac
done

#Apache or Nginx
PS3='Install Apache or Nginx?: '
options=("Apache" "Nginx")
select opt in "${options[@]}"; do
  case $opt in
  "Apache")
    echo "Apache will be installed"
    webserv=Apache
    break
    ;;
  "Nginx")
    echo "Nginx will be installed"
    webserv=Nginx
    break
    ;;
  *) echo "$REPLY is not a valid option" ;;
  esac
done

#Install Certbot?
while true; do
  read -p "Should Certbot be installed?" yn
  case $yn in
  [Yy]*)
    certbot=yes
    echo "Certbot will be installed"
    break
    ;;
  [Nn]*)
    certbot=no
    echo "Certbot will NOT be installed"
    break
    ;;
  *) echo "Please answer yes or no." ;;
  esac
done

#PHP 7.1, 7.2, or 7.3?
PS3='Which PHP Version should be installed?: '
options=("7.1" "7.2" "7.3")
select opt in "${options[@]}"; do
  case $opt in
  "7.1")
    echo "PHP 7.1 will be installed"
    phpver=71
    break
    ;;
  "7.2")
    echo "PHP 7.1 will be installed"
    phpver=72
    break
    ;;
  "7.3")
    echo "PHP 7.1 will be installed"
    phpver=73
    break
    ;;
  *) echo "$REPLY is not a valid option" ;;
  esac
done

# TODO: Specify SQL Versions
#MySQL, MariaDB, or Postgres?
PS3='Which Database Management System should installed?: '
options=("MySQL" "MariaDB" "Postgres" "None")
select opt in "${options[@]}"; do
  case $opt in
  "MySQL")
    echo "MySQL will be installed"
    dbms="MySQL"
    break
    ;;
  "MariaDB")
    echo "MariaDB will be installed"
    dbms="MariaDB"
    break
    ;;
  "Postgres")
    echo "Postgres will be installed"
    dbms="Postgres"
    break
    ;;
  "None")
    echo "No DBMS will be installed"
    dbms="None"
    break
    ;;
  *) echo "$REPLY is not a valid option" ;;
  esac
done

#Install phpMyAdmin, phpPgAdmin?
if [ "$dbms" == 'MySQL' ] || [ "$dbms" == 'MariaDB' ]; then
  while true; do
    read -p "Do you want to install phpMyAdmin?" yn
    case $yn in
    [Yy]*)
      dbmsgui=yes
      echo "phpMyAdmin will be installed"
      break
      ;;
    [Nn]*)
      dbmsgui=no
      echo "phpMyAdmin will not be installed"
      break
      ;;
    *) echo "Please answer yes or no." ;;
    esac
  done
else
  while true; do
    read -p "Do you want to install phpPgAdmin?" yn
    case $yn in
    [Yy]*)
      dbmsgui=yes
      echo "phpPgAdmin will be installed"
      break
      ;;
    [Nn]*)
      dbmsgui=no
      echo "phpPgAdmin will not be installed"
      break
      ;;
    *) echo "Please answer yes or no." ;;
    esac
  done
fi

#Install Redis?
while true; do
  read -p "Do you want to install Redis?" yn
  case $yn in
  [Yy]*)
    redis=yes
    echo "Redis will be installed"
    break
    ;;
  [Nn]*)
    redis=no
    echo "Redis will not be installed"
    break
    ;;
  *) echo "Please answer yes or no." ;;
  esac
done

#Install Redis Commander software?
if [ ${redis} == yes ]; then
  while true; do
    read -p "Do you want to install Redis Commander Web GUI?" yn
    case $yn in
    [Yy]*)
      rediscommander=yes
      echo "Redis Commander Web GUI will be installed"
      break
      ;;
    [Nn]*)
      rediscommander=no
      echo "Redis Commander Web GUI will not be installed"
      break
      ;;
    *) echo "Please answer yes or no." ;;
    esac
  done
fi

#What should the hostname of the server be?
echo -n "Enter your hostname for the server and press [ENTER]: "
read hostname

#What user should run services? (EX: appuser)
echo -n "Enter the username you want to run services as and press [ENTER]: "
read linuxuser

#Password for Database Server
echo -n "Enter the password for ${dbms} and press [ENTER]: "
read dbmspass

#Password for Redis
echo -n "Enter the password for Redis and press [ENTER]: "
read redispass

#App Directory?
echo -n "Enter the directory you want to store your apps in and press [ENTER]: "
read appdir

#Log Directory?
echo -n "Enter the directory you want to store logs in and press [ENTER]: "
read logdir

#Config Directory?
echo -n "Enter the directory you want to store config files in and press [ENTER]: "
read configdir

echo "Your configuration is as follows: "
echo "You are running ${OS} ${VER}"
echo "SElinux will be enabled: ${selinux}"
echo "Firewall will be enabled: ${firewall}"
echo "Webserver will be: ${webserv}"
echo "Certbot will be installed: ${certbot}"
echo "PHP will be installed as version: ${phpver}"
echo "Your database management system will be: ${dbms}"
if [ "$dbms" == 'MySQL' ] || [ "$dbms" == 'MariaDB' ]; then
    echo "phpMyAdmin will be installed: ${dbmsgui}"
else
    echo "phpPgAdmin will be installed: ${dbmsgui}"
fi
echo "Redis will be installed: ${redis}"
echo "Redis Commander will be installed: ${rediscommander}"
echo "Your hostname will be: ${hostname}"
echo "Your services will run as: ${linuxuser}"
echo "Your applications will be stored in: ${appdir}"
echo "Your logs will be stored in: ${logdir}"
echo "Your configurations will be stored in: ${configdir}"

while true; do
  read -p "Do you wish to proceed and configure this server with the above settings?" yn
  case $yn in
  [Yy]*)
    echo "Starting Installation"
    break
    ;;
  [Nn]*)
    echo "Aborting Installation. No changes have occured."
    exit 0
    ;;
  *) echo "Please answer yes or no." ;;
  esac
done

# Setup Functions
checkForError() {
  if [ $? -eq 0 ]; then
    echo "[OK]"
  else
    echo "[FAIL]"
    exit $?
  fi
}

if [ "$OS" == 'CentOS Linux' ] && [ "$VER" == 7 ]; then
  #  Update OS
  echo "Updating Operating System"
  yum update -y
  checkForError

  #  Install basic tools
  echo "Installing EPEL Release"
  yum install epel-release -y
  checkForError

  echo "Installing Basic Tools"
  yum install nano htop wget curl git yum-utils unzip -y
  checkForError
  yum install -y http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y
  checkForError

# TODO: Set contexts for all services
  # SELinux Configuration
  if [ ${selinux} == yes ]; then
    echo "Enabling SELinux"
    setenforce 1
    echo "SELINUX=enforcing" >/etc/sysconfig/selinux
    checkForError
    echo "SELINUXTYPE=targeted" >>/etc/sysconfig/selinux
    checkForError
  else
    echo "Disabling SELinux"
    setenforce 0
    echo "SELINUX=disabled" >/etc/sysconfig/selinux
    checkForError
  fi

# TODO: Allow traffic for all services
  #  Firewall Configuration
  if [ ${selinux} == yes ]; then
    echo "Enabling Firewall"
    systemctl enable firewalld
    checkForError
    systemctl start firewalld
    checkForError
  else
    echo "Disabling Firewall"
    systemctl disable firewalld
    checkForError
    systemctl stop firewalld
    checkForError
  fi

  #  Set hostname
  echo "Setting Hostname"
  hostnamectl set-hostname ${hostname}
  checkForError
  echo "Hostname is ${hostname}"

# TODO: Add user to correct groups
  #  Add new linux user
  echo "Adding new user"
  useradd ${linuxuser}
  checkForError

  #  Create Application Directory
  echo "Creating Application Directory"
  mkdir -p ${appdir}

  #  Create Log Directory
  echo "Creating Log Directory"
  mkdir -p ${logdir}

  #  Create Configuration Directory
  echo "Creating Configuration Directory"
  mkdir -p ${configdir}

  #  Install Webserver
  if [ ${webserv} == Apache ]; then
    echo "Installing Apache"
    yum update httpd -y
    checkForError
    yum install httpd -y
    checkForError
    systemctl start httpd
    checkForError
    systemctl status httpd
    checkForError
  else
    echo "Installing Nginx"
    yum install nginx -y
    checkForError
    systemctl disable httpd
    checkForError
    systemctl start nginx
    checkForError
    systemctl enable nginx
    checkForError
  fi

  #  Install Certbot
  if [ ${certbot} == yes ]; then
    echo "Installing Certbot"
    #  Install Additional Repo for Certbot
    yum -y install yum-utils
    checkForError
    yum-config-manager --enable rhui-REGION-rhel-server-extras rhui-REGION-rhel-server-optional
    checkForError
    yum install certbot -y
    checkForError
  fi

#TODO: Install PHP
  #  Install PHP
  echo "Installing PHP"
  yum-config-manager --enable remi-php${phpver}
  sudo yum install php php-common php-opcache php-mcrypt php-cli php-gd php-curl php-mysqlnd php-bcmath php-ctype php-json php-mbstring php-openssl php-pdo php-xml -y

#TODO: Install DBMS
  #  Install DBMS
  if [ ${dbms} != "None" ]; then
    echo "Installing Database Management System"
    if [ ${dbms} != "MySQL" ]; then
      echo "Installing MySQL as DBMS"
    elif [ ${dbms} != "MariaDB" ]; then
      echo "Installing MariaDB as DBMS"
    elif [ ${dbms} != "Postgres" ]; then
      echo "Installing Postgres as DBMS"
    fi
  else
    echo "An unknown error occured. Unable to determine which DBMS to install"
    exit 1
  fi

#TODO: Configure DBMS
  #  Install DBMS GUI
  if [ ${dbmsgui} == yes ]; then
    echo "Installing Database Management System GUI"
    if [ "$dbms" == 'MySQL' ] || [ "$dbms" == 'MariaDB' ]; then
        yum -y install phpMyAdmin
        checkForError
    elif [ "$dbms" == 'Postgres' ]; then
      yum -y install phpPgAdmin
      checkForError
    fi
  fi

#TODO: Configure Redis
  #  Install Redis
  if [ ${redis} == yes ]; then
    echo "Installing Redis"
    yum install redis -y
    checkForError
  fi

  # Install Composer
  echo "Installing Composer"
  source ~/.bash_profile
  php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
  checkForError
  php -r "if (hash_file('sha384', 'composer-setup.php') === 'a5c698ffe4b8e849a443b120cd5ba38043260d5c4023dbf93e1558871f1f07f58274fc6f4c93bcfd858c6bd0775cd8d1') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
  checkForError
  php composer-setup.php
  checkForError
  php -r "unlink('composer-setup.php');"
  checkForError

  #  Make Composer globally available
  echo "Making Composer Globally Avaliable"
  mv composer.phar /usr/local/bin/composer
  checkForError
  source ~/.bash_profile
  composer -v

  # Install Node
  echo "Installing N Node Version Manager"
  curl -L https://git.io/n-install | bash -s -- -y
  checkForError
  source ~/.bash_profile
  echo "Installing Latest node.js Version"
  n latest
  checkForError
  node -v
  npm -v

  # Install Laravel Tools
  echo "Installing Laravel Installer"
  composer global require laravel/installer
  checkForError

  #  Install Redis Commander
  if [[ ${redisgui} == yes ]]; then
    echo "Installing Redis Commander"
    npm install -g redis-commander --unsafe
    checkForError
  fi

#  All finished, reboot server
  echo "Rebooting server in 10 seconds"
  sleep 10;
  reboot now
fi
